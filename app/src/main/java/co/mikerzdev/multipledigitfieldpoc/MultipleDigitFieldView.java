package co.mikerzdev.multipledigitfieldpoc;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Arrays;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class MultipleDigitFieldView extends LinearLayout {

    //final String NO_WIDTH_SPACE = "\u200B";
    private static final String NO_WIDTH_SPACE = "$";
    private static final CharSequence EMPTY_STRING = "";
    private static final int FIRST_FIELD_POS = 0;
    private static final int PASTE_ID = 158;

    private GestureDetector gestureDetector;
    private PopupMenu pasteMenu;
    private int currentFocusedFieldPos = 0;

    @BindViews({R.id.multipledigitfield_edittext_0, R.id.multipledigitfield_edittext_1, R.id.multipledigitfield_edittext_2,
            R.id.multipledigitfield_edittext_3, R.id.multipledigitfield_edittext_4, R.id.multipledigitfield_edittext_5,
            R.id.multipledigitfield_edittext_6})
    List<MultipleDigitEditText> fieldList;

    public MultipleDigitFieldView(Context context) {
        super(context);
    }

    public MultipleDigitFieldView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.custom_view_multiple_digit_field, this);
        ButterKnife.bind(this);
        enableField(FIRST_FIELD_POS);
        setupFields();
        setupClickEventsListener();
        setupPastePopupMenu();

    }

    private void setupClickEventsListener() {
        gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent ev) {
                setFocusToLastAvailableField();
                return true;
            }

            @Override
            public void onLongPress(MotionEvent ev) {
                pasteMenu.show();
            }
        });
    }

    private void setFocusToLastAvailableField() {
        MultipleDigitEditText field;
        for (int i = getFieldsCount() - 1; i >= 0; i--) {
            field = fieldList.get(i);
            if (field.isEnabled()) {
                field.requestFocus();
                break;
            }
        }
    }

    private void setupPastePopupMenu() {
        pasteMenu = new PopupMenu(getContext(), this);
        pasteMenu.getMenu().add(Menu.NONE, PASTE_ID, Menu.NONE, android.R.string.paste);
        pasteMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == PASTE_ID) {
                    handlePastedData();
                }
                return false;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        gestureDetector.onTouchEvent(ev);
        return false;
    }


    public MultipleDigitFieldView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void enableField(int field) {
        MultipleDigitEditText editField = fieldList.get(field);

        if (!editField.isEnabled()) {
            editField.setEnabled(true);
        }

        editField.setTextIgnoringTextWatcher(NO_WIDTH_SPACE);
        editField.requestFocus();
    }

    private void disableField(int field) {
        fieldList.get(field).setTextIgnoringTextWatcher(EMPTY_STRING);
        fieldList.get(field).setEnabled(false);
    }

    private void handlePastedData() {
        String pastedString = getPastedString();
        if (!isValidPasteText(pastedString)) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        MultipleDigitEditText field;
        Character[] pasteArray = getPasteArray(pastedString);

        for (int i = 0; i < getFieldsCount(); i++) {
            field = fieldList.get(i);

            if (pasteArray[i] == null) {
                disableField(i);
            } else {
                field.setEnabled(true);
                sb.append(NO_WIDTH_SPACE).append(pasteArray[i].toString());
                field.setTextIgnoringTextWatcher(sb.toString());
                sb.setLength(0);
            }
        }

        if (pastedString.length() < getFieldsCount()) {
            enableField(pastedString.length());
        } else {
            fieldList.get(getFieldsCount() - 1).requestFocus();
        }

    }

    private Character[] getPasteArray(String pastedString) {
        Character[] pasteArray = new Character[getFieldsCount()];
        Arrays.fill(pasteArray, null);
        char[] textArray = pastedString.toCharArray();
        for (int i = 0; i < textArray.length; i++) {
            pasteArray[i] = textArray[i];
        }
        return pasteArray;
    }

    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (MultipleDigitEditText field : fieldList) {
            stringBuilder.append(field.getText());
        }
        String reservationNumber = stringBuilder.toString().replace(NO_WIDTH_SPACE, EMPTY_STRING);

        return reservationNumber;
    }


    private void setupFields() {
        for (MultipleDigitEditText editText : fieldList) {
            editText.setOnFieldTextChangedListener(new MultipleDigitEditText.OnFieldTextChangedListener() {
                @Override
                public void onDeleteDigit() {
                    onDeleteDigitImpl();
                }

                @Override
                public void onNewDigit() {
                    onNewDigitImpl();
                }

                @Override
                public void onNextDigit(String nextDigit) {
                    onNextDigitImpl(nextDigit);
                }
            });

            editText.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        setCurrentFocus(view.getId());
                    }

                }
            });
        }
    }

    private void setCurrentFocus(int viewId) {
        for (MultipleDigitEditText field : fieldList) {
            if (viewId == field.getId()) {
                currentFocusedFieldPos = fieldList.indexOf(field);
                break;
            }
        }
    }

    private void onDeleteDigitImpl() {
        if (getEnabledFieldsCount() - 1 == currentFocusedFieldPos) {
            if (currentFocusedFieldPos == FIRST_FIELD_POS) {
                fieldList.get(FIRST_FIELD_POS).setTextIgnoringTextWatcher(NO_WIDTH_SPACE);
            } else if (!isFinalEmptySpace()) {
                fieldList.get(currentFocusedFieldPos - 1).setTextIgnoringTextWatcher(NO_WIDTH_SPACE);
                fieldList.get(currentFocusedFieldPos - 1).requestFocus();
                disableField(currentFocusedFieldPos + 1);
            }

        } else {
            for (int i = currentFocusedFieldPos + 1; i < getEnabledFieldsCount(); i++) {
                fieldList.get(i - 1).setTextIgnoringTextWatcher(fieldList.get(i).getText());
            }
            if (currentFocusedFieldPos > FIRST_FIELD_POS) {
                fieldList.get(currentFocusedFieldPos - 1).requestFocus();
            }

            if (getFilledFieldsCount() == getFieldsCount()) {
                fieldList.get(getEnabledFieldsCount() - 1).setTextIgnoringTextWatcher(NO_WIDTH_SPACE);
            } else {
                disableField(getEnabledFieldsCount() - 1);
            }

        }

    }

    private void onNewDigitImpl() {
        if (getEnabledFieldsCount() < getFieldsCount())
            enableField(currentFocusedFieldPos + 1);

    }


    private boolean isFinalEmptySpace() {
        return getEnabledFieldsCount() == getFieldsCount() && fieldList.get(getEnabledFieldsCount() - 1)
                .getText().toString().equals(NO_WIDTH_SPACE);
    }

    private void onNextDigitImpl(String nextDigit) {
        if (getFilledFieldsCount() < getFieldsCount()) {
            StringBuilder sb = new StringBuilder();
            MultipleDigitEditText nextField;

            for (int i = getFilledFieldsCount(); i > currentFocusedFieldPos; i--) {
                if (i + 1 < getFieldsCount()) {
                    nextField = fieldList.get(i + 1);
                } else {
                    nextField = fieldList.get(i);
                }
                if (!nextField.isEnabled()) {
                    nextField.setEnabled(true);
                }
                nextField.setTextIgnoringTextWatcher(fieldList.get(i).getText());
            }
            sb.append(NO_WIDTH_SPACE).append(nextDigit);
            fieldList.get(currentFocusedFieldPos + 1).setTextIgnoringTextWatcher(sb.toString());
            fieldList.get(currentFocusedFieldPos + 1).requestFocus();
            sb.setLength(0);
        }

    }

    private int getFilledFieldsCount() {
        int count = 0;
        for (MultipleDigitEditText field : fieldList) {
            if (field.getText().length() == 2) {
                count++;
            }
        }

        return count;
    }

    private int getFieldsCount() {
        if (fieldList == null) {
            return 0;
        } else {
            return fieldList.size();
        }

    }

    private int getEnabledFieldsCount() {
        int enabledFieldsCount = 0;
        for (MultipleDigitEditText field : fieldList) {
            if (field.isEnabled())
                enabledFieldsCount++;
        }
        return enabledFieldsCount;
    }

    private String getPastedString() {
        ClipboardManager clipboard =
                (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = clipboard.getPrimaryClip();

        if (clip == null || clip.getItemAt(0) == null
                || clip.getItemAt(0).coerceToStyledText(getContext()) == null)
            return EMPTY_STRING.toString();

        CharSequence paste = clip.getItemAt(0).coerceToStyledText(getContext());
        return (isValidPasteText(paste)) ? paste.toString() : EMPTY_STRING.toString();
    }

    private boolean isValidPasteText(CharSequence paste) {
        return paste != null && !paste.toString().isEmpty() && TextUtils.isDigitsOnly(paste) && paste.length() <= 7;
    }
}
