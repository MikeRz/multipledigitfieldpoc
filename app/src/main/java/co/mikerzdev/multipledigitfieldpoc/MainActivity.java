package co.mikerzdev.multipledigitfieldpoc;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.container)
    FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        container.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    container.setBackgroundColor(ContextCompat.getColor(MainActivity.this,android.R.color.holo_red_dark));
                } else {
                    container.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorPrimary));
                }

            }
        });

    }

}
