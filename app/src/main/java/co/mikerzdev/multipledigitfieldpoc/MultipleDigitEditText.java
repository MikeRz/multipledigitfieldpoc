package co.mikerzdev.multipledigitfieldpoc;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class MultipleDigitEditText extends android.support.v7.widget.AppCompatEditText {

    private TextWatcher textWatcher;
    private OnBackPressedListener onBackPressedListener;
    private OnFieldTextChangedListener onFieldTextChangedListener;

    public MultipleDigitEditText(Context context) {
        super(context);
    }

    public MultipleDigitEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTextWatcher();
        addTextChangedListener(textWatcher);

    }

    public MultipleDigitEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        String text = getText().toString();
        //TODO: use stringutils
        if (text != null && !text.isEmpty()) {
            setSelection(text.length());
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        //TODO: use stringutils
        if (text != null && !text.toString().isEmpty())
            setSelection(text.length());
    }

    public void setOnFieldTextChangedListener(OnFieldTextChangedListener onFieldTextChangedListener) {
        this.onFieldTextChangedListener = onFieldTextChangedListener;
    }


    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (wasBackKeyUpPressed(keyCode, event)) {
            if (onBackPressedListener != null)
            onBackPressedListener.onBackPressed();
            return false;
        }
        return super.dispatchKeyEvent(event);
    }

    private boolean wasBackKeyUpPressed(int keyCode, KeyEvent event) {
        return (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP);
    }

    public void setTextIgnoringTextWatcher(CharSequence charSequence) {
        removeTextChangedListener(textWatcher);
        setText(charSequence);
        addTextChangedListener(textWatcher);
    }

    private void initTextWatcher() {
        textWatcher = new TextWatcher() {
            int beforeTextLength;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                beforeTextLength = charSequence.length();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (beforeTextLength > charSequence.length()) {
                    if (onFieldTextChangedListener != null)
                        onFieldTextChangedListener.onDeleteDigit();

                } else if (beforeTextLength < charSequence.length()) {
                    if (charSequence.length() > 2) {
                        String nextDigit = Character.toString(charSequence.charAt(2));
                        setTextIgnoringTextWatcher(charSequence.subSequence(0, 2));

                        if (onFieldTextChangedListener != null) {
                            onFieldTextChangedListener.onNextDigit(nextDigit);
                        }
                    } else {
                        if (onFieldTextChangedListener != null)
                            onFieldTextChangedListener.onNewDigit();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }


    public interface OnFieldTextChangedListener {
        void onDeleteDigit();
        void onNewDigit();
        void onNextDigit(String nextDigit);
    }

    public interface OnBackPressedListener {
        void onBackPressed();
    }
}



